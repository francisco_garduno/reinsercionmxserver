package reinsercionserver.auth

import auth.User
import auth.UserRole
import grails.plugin.springsecurity.annotation.Secured
import grails.rest.*
import grails.converters.*
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder

class LoginController {
    static responseFormats = ['json', 'xml']

    def towFactorAuthService
    def springSecurityService
    def tokenService;

    def index() { }

    @Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def towFactorAuth(){
        //Miscellaneous
        //grailsApplication.config.getProperty('filesSave.path')
        //springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
        def params = request.JSON
        def result = [:]
        def user = User.findByUsername(params.username)
        String secretKey = user.key2fa
        String code = params.code
        if (code.equals(towFactorAuthService.getTOTPCode(secretKey))) {
            result.autenticated = true

        } else {
            result.autenticated = false
        }

        respond result
    }

    @Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def key2fa(){
        //println towFactorAuthService.generateSecretKey()
        def params = request.JSON
        def user = User.findByUsername(params.username)
        String secretKey = user.key2fa
        String count =  user.username
        //String count = "${user.username} - ${user.email}"
        String companyName = "Reinsercion"
        String barCodeUrl = towFactorAuthService.getGoogleAuthenticatorBarCode(secretKey, count, companyName);
        def result =[:]
        result.key = user?.key2fa
        result.qr = towFactorAuthService.createQRCode(barCodeUrl, 400, 400)
        respond result
    }

}

