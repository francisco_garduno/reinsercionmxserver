package reinsercionserver

class UrlMappings {

    static mappings = {
        delete "/$controller/$id(.$format)?"(action:"delete")
        get "/$controller(.$format)?"(action:"index")
        get "/$controller/$id(.$format)?"(action:"show")
        post "/$controller(.$format)?"(action:"save")
        put "/$controller/$id(.$format)?"(action:"update")
        patch "/$controller/$id(.$format)?"(action:"patch")

        "/"(controller: 'application', action:'index')
        "500"(view: '/error')
        "404"(view: '/notFound')

        group("/api"){
            //Catálogos estáticos
            "/listarPaises/"(controller: 'catalogo', action:'listarPaises', method: 'GET')
            "/listarEstados/"(controller: 'catalogo', action:'listarEstados', method: 'GET')
            "/listarMunicipios/"(controller: 'catalogo', action:'listarMunicipios', method: 'GET')
            "/listarTipoCentros/"(controller: 'catalogo', action:'listarTipoCentros', method: 'GET')
            "/listarGradosEstudio/"(controller: 'catalogo', action:'listarGradosEstudio', method: 'GET')
            "/listarReligiones/"(controller: 'catalogo', action:'listarReligiones', method: 'GET')
            "/listarEstadosCiviles/"(controller: 'catalogo', action:'listarEstadosCiviles', method: 'GET')
            "/listarOcupaciones/"(controller: 'catalogo', action:'listarOcupaciones', method: 'GET')
            "/listarRoles/"(controller: 'catalogo', action:'listarRoles', method: 'GET')

            //Centro penitenciario
            "/registrarCentroPenitenciario/"(controller: 'centroPenitenciario', action:'registrarCentroPenitenciario', method: 'POST')
            "/listarCentrosPenitenciarios/"(controller: 'centroPenitenciario', action:'listarCentrosPenitenciarios', method: 'GET')
            "/actualizarEstatusCentroPenitenciario/"(controller: 'centroPenitenciario', action:'actualizarEstatusCentroPenitenciario', method: 'GET')

            //Catálogos administrables
            "/registrarModalidadDelito/"(controller: 'catalogo', action:'registrarModalidadDelito', method: 'POST')
            "/listarModalidadesDelito/"(controller: 'catalogo', action:'listarModalidadesDelito', method: 'GET')
            "/actualizarEstatusModalidadDelito/"(controller: 'catalogo', action:'actualizarEstatusModalidadDelito', method: 'GET')
            "/eliminarModalidadDelito/"(controller: 'catalogo', action:'eliminarModalidadDelito', method: 'GET')

            "/registrarTipoDelito/"(controller: 'catalogo', action:'registrarTipoDelito', method: 'POST')
            "/listarTipoDelitos/"(controller: 'catalogo', action:'listarTipoDelitos', method: 'GET')
            "/actualizarEstatusTipoDelito/"(controller: 'catalogo', action:'actualizarEstatusTipoDelito', method: 'GET')

            "/registrarTipoLibertad/"(controller: 'catalogo', action:'registrarTipoLibertad', method: 'POST')
            "/listarTipoLibertades/"(controller: 'catalogo', action:'listarTipoLibertades', method: 'GET')
            "/actualizarEstatusTipoLibertad/"(controller: 'catalogo', action:'actualizarEstatusTipoLibertad', method: 'GET')

            "/registrarClasificacionJuridica/"(controller: 'catalogo', action:'registrarClasificacionJuridica', method: 'POST')
            "/listarClasificacionesJuridicas/"(controller: 'catalogo', action:'listarClasificacionesJuridicas', method: 'GET')
            "/actualizarEstatusClasificacionJuridica/"(controller: 'catalogo', action:'actualizarEstatusClasificacionJuridica', method: 'GET')

            "/registrarEnfermedadCronica/"(controller: 'catalogo', action:'registrarEnfermedadCronica', method: 'POST')
            "/listarEnfermedadesCronicas/"(controller: 'catalogo', action:'listarEnfermedadesCronicas', method: 'GET')
            "/actualizarEstatusEnfermedadCronica/"(controller: 'catalogo', action:'actualizarEstatusEnfermedadCronica', method: 'GET')

            "/registrarMotivoReubicacion/"(controller: 'catalogo', action:'registrarMotivoReubicacion', method: 'POST')
            "/listarMotivosReubicacion/"(controller: 'catalogo', action:'listarMotivosReubicacion', method: 'GET')
            "/actualizarEstatusMotivoReubicacion/"(controller: 'catalogo', action:'actualizarEstatusMotivoReubicacion', method: 'GET')

            "/registrarDormitorio/"(controller: 'catalogo', action:'registrarDormitorio', method: 'POST')
            "/listarDormitorios/"(controller: 'catalogo', action:'listarDormitorios', method: 'GET')
            "/actualizarEstatusDormitorio/"(controller: 'catalogo', action:'actualizarEstatusDormitorio', method: 'GET')

            "/registrarTipoActividad/"(controller: 'catalogo', action:'registrarTipoActividad', method: 'POST')
            "/listarTipoActividades/"(controller: 'catalogo', action:'listarTipoActividades', method: 'GET')
            "/actualizarEstatusTipoActividad/"(controller: 'catalogo', action:'actualizarEstatusTipoActividad', method: 'GET')

            "/registrarActividad/"(controller: 'catalogo', action:'registrarActividad', method: 'POST')
            "/listarActividades/"(controller: 'catalogo', action:'listarActividades', method: 'GET')
            "/actualizarEstatusActividad/"(controller: 'catalogo', action:'actualizarEstatusActividad', method: 'GET')

            //Ingreso de imputado
            "/registrarIngresoImputado/"(controller: 'imputado', action:'registrarIngresoImputado', method: 'POST')
            "/consultarIngresoImputado/"(controller: 'imputado', action:'consultarIngresoImputado', method: 'GET')
            "/registrarApodo/"(controller: 'imputado', action:'registrarApodo', method: 'POST')
            "/seleccionarApodoPrincipal/"(controller: 'imputado', action:'seleccionarApodoPrincipal', method: 'GET')

            "/registarPreDelito/"(controller: 'imputado', action:'registarPreDelito', method: 'POST')
            "/registrarReferenciaPersonal/"(controller: 'imputado', action:'registrarReferenciaPersonal', method: 'POST')
            "/listarReferenciasPersonales/"(controller: 'imputado', action:'listarReferenciasPersonales', method: 'GET')




            //Señas particulares
            "/registrarCaracteristicasEspecificas/"(controller: 'caracteristicasEspecificas', action:'registrarCaracteristicasEspecificas', method: 'POST')
            "/listarCaracteristicasEspecificas/"(controller: 'caracteristicasEspecificas', action:'listarCaracteristicasEspecificas', method: 'GET')
            "/findByCaracteristicasEspecificas/"(controller: 'caracteristicasEspecificas', action:'findByCaracteristicasEspecificas', method: 'GET')
            "/eliminarCaracteristicasEspecificas/"(controller: 'caracteristicasEspecificas', action:'eliminarCaracteristicasEspecificas', method: 'GET')

            "/prueba/"(controller: 'prueba', action:'prueba', method: 'GET')


            /*group("/login"){
                "/towFactorAuth/"(controller: 'Login', action:'towFactorAuth', method: 'POST')
                "/key2fa/"(controller: 'Login', action:'key2fa', method: 'POST')
                "/test2fa/"(controller: 'Login', action:'test2fa', method: 'GET')
            }*/
        }
    }
}
