package reinsercionserver.procesos

import catalogosestaticos.EstadoCivil
import catalogosestaticos.GradoEstudio
import catalogosestaticos.Municipio
import catalogosestaticos.Ocupacion
import catalogosestaticos.Pais
import catalogosestaticos.Parentesco
import catalogosestaticos.Religion
import catalogosgestionables.ModalidadDelito
import catalogosgestionables.TipoDelito
import grails.rest.*
import grails.converters.*
import procesos.Apodo
import procesos.CarpetaImputado
import grails.plugin.springsecurity.annotation.Secured
import procesos.CentroPenitenciario
import procesos.Delito
import procesos.Imputado
import procesos.ReferenciaPersonal

@Secured(['ROLE_TEST', 'ROLE_ADMIN'])
class ImputadoController {
	static responseFormats = ['json', 'xml']
	
    def index() { }



    /*
     * Permite registrar los datos de ingreso de un imputado.
     */
    def registrarIngresoImputado(){
        def respuesta = [:]
        def parameters = request.JSON

        println(parameters)
        println(parameters.properties)

        boolean esRegistro = false
        def existeRegistro = Imputado.findByFolioOrNumeroExpedienteOrCurp(parameters.folio, parameters.numeroExpediente, parameters.curp)
        def registroId = (existeRegistro) ? existeRegistro.id : null
        def objetoImputado = (!parameters.id || parameters == "") ? new Imputado() :  Imputado.get(parameters.id)
        if(!parameters.id || parameters == ""){ //Registro nuevo
            esRegistro = true
        }
        //Datos de la carpeta
        if(esRegistro) { //Una vez registrada la carpeta del imputado, sus datos no son modificables
            objetoImputado.folio = "1-ATL-2020"    //parameters.folio
            objetoImputado.numeroExpediente = "1-ATL-2020" //parameters.numeroExpediente

            //Asignar el centro penitenciario al que pertenece el imputado
            objetoImputado.centroPenitenciario = CentroPenitenciario.get(1)
        }else{ //Modificación
            objetoImputado.numeroExpediente = parameters.numeroExpediente
        }

        objetoImputado.numeroControlRenip = parameters.numeroControlRenip
        objetoImputado.tipoImputado = parameters.tipoImputado
        objetoImputado.tipoIngreso = parameters.tipoIngreso
        objetoImputado.clasificacion = parameters.clasificacion

        //Datos generales del imputado
        objetoImputado.curp = (parameters.imputado.curp) ? parameters.imputado.curp : null
        objetoImputado.fechaNacimiento = new Date()
        objetoImputado.edadAparente = parameters.imputado.edadAparente
        objetoImputado.genero = parameters.imputado.genero
        objetoImputado.codigoPostal = parameters.imputado.codigoPostal
        objetoImputado.colonia = parameters.imputado.colonia

        objetoImputado.calleNumero = parameters.imputado.calleNumero
        objetoImputado.numeroHijos = (parameters.imputado.numeroHijos || parameters.imputado.numeroHijos != "") ? parameters.imputado.numeroHijos : 0
        objetoImputado.nombrePadre = parameters.imputado.nombrePadre
        objetoImputado.nombreMadre = parameters.imputado.nombreMadre
        objetoImputado.etnia = parameters.imputado.etnia
        objetoImputado.esIndigina = parameters.imputado.esIndigena
        objetoImputado.hablaIndigena = parameters.imputado.hablaIndigena
        objetoImputado.municipio = Municipio.get(parameters.imputado.municipio.id)
        objetoImputado.paisNacimiento = Pais.get(parameters.imputado.paisNacimiento.id)
        objetoImputado.religion = Religion.get(parameters.imputado.religion.id)
        objetoImputado.estadoCivil = EstadoCivil.get(parameters.imputado.estadoCivil.id)
        objetoImputado.ocupacion = Ocupacion.get(parameters.imputado.ocupacion.id)
        objetoImputado.gradoEstudio = GradoEstudio.get(parameters.imputado.gradoEstudio.id)

        if(esRegistro && !existeRegistro){ //Registro
            try{
                objetoImputado.save(failOnError: true)
                respuesta.error = false
                respuesta.mensaje = "Registro exitoso."

                objetoImputado.folio = "${objetoImputado.id}-2020"
                objetoImputado.numeroExpediente = "${objetoImputado.id}-2020"
                objetoImputado.save(flush: true, failOnError: true)

            }catch(Exception e){
                println(e.printStackTrace())
                respuesta.error = true
                respuesta.mensaje = "Registro fallido."
            }
        }else if((!esRegistro && parameters.id == registroId) || (!esRegistro && !existeRegistro)){ //Actualización
            if (objetoImputado.save(flush: true, failOnError: true)){
                respuesta.error = false
                respuesta.mensaje = "Actualización exitosa."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Actualización fallida."
            }
        }else{ //Registro duplicado
            respuesta.error = true
            respuesta.mensaje = "Registro existente, pruebe con otro <<Folio>>, <<CURP>> o <<Número expediente>>."
        }
        respuesta.idRegistro = (objetoImputado) ? objetoImputado.id : null
        respond respuesta
    }//def



    /*
     * Permite mostrar los datos de ingreo del imputado (Dactiloscopia).
     */
    def consultarIngresoImputado(){
        def respuesta = [:]
        def objetoIngreso = Imputado.get(params.imputadoId)
        def objetoCarpeta = [:]
        respuesta.error = false
        if(objetoIngreso){
            respuesta.mensaje = "Consulta exitosa."
            objetoCarpeta.id = objetoIngreso.id
            objetoCarpeta.folio = objetoIngreso.folio
            objetoCarpeta.numeroExpediente = objetoIngreso.numeroExpediente
            objetoCarpeta.numeroControlRenip = objetoIngreso.numeroControlRenip
            objetoCarpeta.tipoImputado = objetoIngreso.tipoImputado
            objetoCarpeta.tipoIngreso = objetoIngreso.tipoIngreso
            objetoCarpeta.clasificacion = objetoIngreso.clasificacion

            def objetoImputado = [:]
            objetoImputado.curp = objetoIngreso.curp
            objetoImputado.fechaNacimiento = objetoIngreso.fechaNacimiento
            objetoImputado.edadAparente = objetoIngreso.edadAparente
            objetoImputado.genero = objetoIngreso.genero
            objetoImputado.codigoPostal = objetoIngreso.codigoPostal
            objetoImputado.colonia = objetoIngreso.colonia
            objetoImputado.calleNumero = objetoIngreso.calleNumero
            objetoImputado.numeroHijos = objetoIngreso.numeroHijos
            objetoImputado.nombrePadre = objetoIngreso.nombrePadre
            objetoImputado.nombreMadre = objetoIngreso.nombreMadre
            objetoImputado.etnia = objetoIngreso.etnia
            objetoImputado.esIndigina = objetoIngreso.esIndigina
            objetoImputado.hablaIndigena = objetoIngreso.hablaIndigena

            def objetoMunicipio = [:]
            objetoMunicipio.id = objetoIngreso.municipio.id
            objetoMunicipio.nombre = objetoIngreso.municipio.nombre
            def objetoEstadop = [:]
            objetoEstadop.id = objetoIngreso.municipio.estado.id
            objetoEstadop.nombre = objetoIngreso.municipio.estado.nombre
            objetoMunicipio.estado = objetoEstadop
            objetoImputado.municipio = objetoMunicipio

            def objetoPais = [:]
            objetoPais.id = objetoIngreso.paisNacimiento.id
            objetoPais.nombre = objetoIngreso.paisNacimiento.nombre
            objetoImputado.paisNacimiento = objetoPais

            def objetoReligion = [:]
            objetoReligion.id = objetoIngreso.religion.id
            objetoReligion.nombre = objetoIngreso.religion.nombre
            objetoImputado.religion = objetoReligion

            def objetoEstado = [:]
            objetoEstado.id = objetoIngreso.estadoCivil.id
            objetoEstado.nombre = objetoIngreso.estadoCivil.nombre
            objetoImputado.estadoCivil = objetoEstado

            def objetoOcupacion = [:]
            objetoOcupacion.id = objetoIngreso.ocupacion.id
            objetoOcupacion.nombre = objetoIngreso.ocupacion.nombre
            objetoImputado.ocupacion = objetoOcupacion

            def objetoGrado = [:]
            objetoGrado.id = objetoIngreso.gradoEstudio.id
            objetoGrado.nombre = objetoIngreso.gradoEstudio.nombre
            objetoImputado.gradoEstudio = objetoGrado

            def criteriaD = Delito.createCriteria()
            def listaDelito = criteriaD.list {
                imputado{
                    'in'("id", objetoIngreso.id.toLong())
                }
                order("fechaRegistro", "desc")
            }
            def listaD = []
            listaDelito.each { itD ->
                listaD.add(
                        id: itD.id,
                        delito: itD.tipoDelito.nombre,
                        fechaDetencion: itD.fechaDetencion,
                        fechaRegistro: itD.fechaRegistro
                )
            }
            objetoImputado.delitos = listaD

            def criteriaA = Apodo.createCriteria()
            def listaApodo = criteriaA.list {
                imputado{
                    'in'("id", objetoIngreso.id.toLong())
                }
                order("id", "desc")
            }
            def listaA = []
            listaApodo.each { itA ->
                listaA.add(
                        id: itA.id,
                        nombre: itA.nombre,
                        apellidoPaterno: itA.apellidoPaterno,
                        apellidoMaterno: itA.apellidoMaterno,
                        otro: itA.otro,
                        principal: itA.principal
                )
            }
            objetoImputado.apodos = listaA

            objetoCarpeta.imputado = objetoImputado
        }else{
            respuesta.mensaje = "Registro inexistente."
        }
        respuesta.ingreso = objetoCarpeta
        respond respuesta
    }//def



    /*
     * Permite registrar alias a un imputado (máximo 5)
     */
    def registrarApodo(){
        def respuesta = [:]
        def parameters = request.JSON
        println(parameters)
        boolean esRegistro = false
        def objetoImputado = Imputado.get(parameters.imputado.id)
        def listaApodo = Apodo.findAllByImputado(objetoImputado)
        def objetoApodo = (!parameters.id || parameters == "") ? new Apodo() : Apodo.get(parameters.id)
        if(!parameters.id || parameters == ""){ //Registro nuevo
            esRegistro = true
            objetoApodo.fechaRegistro = new Date()
            objetoApodo.imputado = objetoImputado
            //El alias principal, es el primer registro
            if(listaApodo.size() == 0){
                objetoApodo.principal = true
            }
        }
        objetoApodo.nombre = parameters.nombre
        objetoApodo.apellidoPaterno = parameters.apellidoPaterno
        objetoApodo.apellidoMaterno = parameters.apellidoMaterno
        objetoApodo.otro = parameters.otro
        if(esRegistro && listaApodo.size() <= 4){ //Registro
            if (objetoApodo.save(flush: true, failOnError: true)){
                respuesta.error = false
                respuesta.mensaje = "Registro exitoso."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Registro fallido."
            }
        }else if (!esRegistro){ //Actualización
            if (objetoApodo.save(flush: true)){
                respuesta.error = false
                respuesta.mensaje = "Actualización exitosa."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Actualización fallida."
            }
        }else{
            respuesta.error = true
            respuesta.mensaje = "Registro fallido. Se ha superado el número máximo de alias registros para el imputado."
        }
        respuesta.apodo = objetoApodo
        respond respuesta
    }//def



    /*
     * Permite seleccionar un apodo como principal
     */
    def seleccionarApodoPrincipal(){
        def respuesta = [:]
        respuesta.error = true
        def objetoApodo = Apodo.get(params.apodoId)
        if(objetoApodo){
            def listaApodo = Apodo.findAllByImputado(objetoApodo.imputado)
            try{
                //Quitar el estatus de principal al registro anterior
                listaApodo.each {
                    if(it.principal){
                        it.principal = false
                        it.save(flush: true, failOnError: true)
                    }
                }
                //Seleccionar el nuevo apodo como principal
                objetoApodo.principal = true
                objetoApodo.save(flush: true, failOnError: true)
                respuesta.error = false
                respuesta.mensaje = "Actualización exitosa."
            }catch(Exception e){
                respuesta.mensaje = "Actualización fallida."
            }
        }else{
            respuesta.mensaje = "Registro inexistente."
        }
        respond respuesta
    }//def



    /*
     * Permite registrar y actualizar el pre-registro de un delito
     */
    def registarPreDelito(){
        def respuesta = [:]
        def parameters = request.JSON
        boolean esRegistro = false
        def objetoDelito = (!parameters.id || parameters == "") ? new Delito() : Delito.get(parameters.id)
        if(!parameters.id || parameters == ""){ //Registro nuevo
            esRegistro = true
            def objetoImputado = Imputado.get(parameters.imputado.id)
            objetoDelito.fechaRegistro = new Date()
            objetoDelito.fechaDetencion = new Date()
            objetoDelito.imputado = objetoImputado

        }
        objetoDelito.tipoDelito = TipoDelito.get(parameters.tipoDelito.id)
        objetoDelito.causaPenal = parameters.causaPenal
        objetoDelito.carpetaInvestigacion = parameters.carpetaInvestigacion

        if(esRegistro){ //Registro
            if (objetoDelito.save(flush: true, failOnError: true)){
                respuesta.error = false
                respuesta.mensaje = "Registro exitoso."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Registro fallido."
            }
        }else{ //Actualización
            if (objetoDelito.save(flush: true)) {
                respuesta.error = false
                respuesta.mensaje = "Actualización exitosa."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Actualización fallida."
            }
        }
        respond respuesta
    }//def



    /*
     * Permite registrar y actualizar una referencia personal del imputado
     */
    def registrarReferenciaPersonal() {
        def respuesta = [:]
        def parameters = request.JSON
        boolean esRegistro = false
        def objetoReferencia = (!parameters.id || parameters == "") ? new ReferenciaPersonal() : ReferenciaPersonal.get(parameters.id)
        if (!parameters.id || parameters == "") { //Registro nuevo
            esRegistro = true
            def objetoImputado = Imputado.get(parameters.imputado.id)
            objetoReferencia.imputado = objetoImputado
        }
        objetoReferencia.nombre = parameters.nombre
        objetoReferencia.apellidoPaterno = parameters.apellidoPaterno
        objetoReferencia.apellidoMaterno = parameters.apellidoMaterno
        objetoReferencia.genero = parameters.genero
        objetoReferencia.estaVivo = parameters.estaVivo
        objetoReferencia.telefono = parameters.telefono
        objetoReferencia.numeroDependientes = parameters.numeroDependientes
        objetoReferencia.calleNumero = parameters.calleNumero
        objetoReferencia.colonia = parameters.colonia
        objetoReferencia.codigoPostal = parameters.codigoPostal
        objetoReferencia.paisNacimiento = Pais.get(parameters.paisNacimiento.id)
        objetoReferencia.municipio = Municipio.get(parameters.municipio.id)
        objetoReferencia.ocupacion = Ocupacion.get(parameters.ocupacion.id)
        objetoReferencia.parentesco = Parentesco.get(parameters.parentesco.id)
        if(esRegistro){ //Registro
            if (objetoReferencia.save(flush: true, failOnError: true)){
                respuesta.error = false
                respuesta.mensaje = "Registro exitoso."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Registro fallido."
            }
        }else { //Actualización
            if (objetoReferencia.save(flush: true)) {
                respuesta.error = false
                respuesta.mensaje = "Actualización exitosa."
            } else {
                respuesta.error = true
                respuesta.mensaje = "Actualización fallida."
            }
        }
        respond respuesta
    }//def



    /*
     * Permite mostrar el listado de las referencias personales de un imputado en específico.
     */
    def listarReferenciasPersonales(){
        def respuesta = [:]
        def criteria = ReferenciaPersonal.createCriteria()
        def listaReferencia = criteria.list {
            imputado{
                'in'("id", params.imputadoId.toLong())
            }
            order("id", "desc")
        }
        def lista = []
        listaReferencia.each {
            def objetoReferencia = [:]
            objetoReferencia.id = it.id
            objetoReferencia.nombre = it.nombre
            objetoReferencia.apellidoPaterno = it.apellidoPaterno
            objetoReferencia.apellidoMaterno = it.apellidoMaterno
            objetoReferencia.genero = it.genero
            objetoReferencia.estaVivo = it.estaVivo
            objetoReferencia.telefono = it.telefono
            objetoReferencia.numeroDependientes = it.numeroDependientes
            objetoReferencia.calleNumero = it.calleNumero
            objetoReferencia.colonia = it.colonia
            objetoReferencia.codigoPostal = it.codigoPostal

            def objetoPais = [:]
            objetoPais.id = it.paisNacimiento.id
            objetoPais.nombre = it.paisNacimiento.nombre
            objetoReferencia.paisNacimiento = objetoPais

            def objetoMunicipio = [:]
            objetoMunicipio.id = it.municipio.id
            objetoMunicipio.nombre = it.municipio.nombre
            objetoReferencia.municipio = objetoMunicipio

            def objetoOcupacion = [:]
            objetoOcupacion.id = it.ocupacion.id
            objetoOcupacion.nombre = it.ocupacion.nombre
            objetoReferencia.ocupacion = objetoOcupacion

            def objetoParentesco = [:]
            objetoParentesco.id = it.parentesco.id
            objetoParentesco.nombre = it.parentesco.nombre
            objetoReferencia.parentesco = objetoParentesco
            lista << objetoReferencia
        }
        respuesta.error = false
        respuesta.mensaje = (listaReferencia) ? "Consulta exitosa." : "Sin registros."
        respuesta.referenciasPersonales = lista
        respond respuesta
    }//def
}
