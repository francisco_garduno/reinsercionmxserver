package reinsercionserver.procesos

import ch.qos.logback.core.net.server.Client
import grails.rest.*
import grails.converters.*
import grails.plugin.springsecurity.annotation.Secured


@Secured(['ROLE_TEST'])
class PruebaController {
	static responseFormats = ['json', 'xml']
	
    def index() { } //Prueba

    def prueba(){
        def respuesta = [:]
        respuesta.mensaje = "Mensaje mostrado exitosamente"




        respond respuesta
    }

}
