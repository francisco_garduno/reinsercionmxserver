package reinsercionserver.procesos

import auth.Role
import catalogosestaticos.EstadoCivil
import catalogosestaticos.GradoEstudio
import catalogosestaticos.Ocupacion
import catalogosestaticos.Religion
import catalogosestaticos.TipoCentro
import catalogosgestionables.Actividad
import catalogosgestionables.ClasificacionJuridica
import catalogosgestionables.Dormitorio
import catalogosgestionables.EnfermedadCronica
import catalogosgestionables.ModalidadDelito
import catalogosgestionables.MotivoReubicacion
import catalogosgestionables.TipoActividad
import catalogosgestionables.TipoDelito
import catalogosgestionables.TipoLibertad
import catalogosestaticos.Estado
import catalogosestaticos.Municipio
import catalogosestaticos.Pais
import grails.plugin.springsecurity.annotation.Secured
import procesos.CentroPenitenciario

@Secured(['ROLE_TEST', 'ROLE_ADMIN'])
class CatalogoController {

    static responseFormats = ['json', 'xml']

    def userService



    /*
     * Permite mostrar el listado completo de los países
     */
    def listarPaises(){
        def respuesta = [:]
        def criteria = Pais.createCriteria()
        def listaPais = criteria.list {
            eq("estatus", true)
            order("nombre", "asc")
        }
        if(listaPais){
            respuesta.error = false
            respuesta.mensaje = "Consulta exitosa."
        }else{
            respuesta.error = false
            respuesta.mensaje = "Sin registros."
        }
        respuesta.paises = listaPais
        respond respuesta
    }//def



    /*
     * Permite obtener el listado de los estados de México o de un país específico.
     */
    def listarEstados(){
        def respuesta = [:]
        def criteria = Estado.createCriteria()
        def listaEstado =  criteria.list {
            if(params.region == "seleccionada"){ //Los estados del país seleccionado
                pais{
                    'in'("id", params.paisId.toLong())
                }
            }else if(params.region == "mexico"){ //Solo los estados de México
                pais{
                    'in'("id", 142.toLong())
                }
            }
            order("nombre", "asc")
        }
        def lista = []
        if(listaEstado){
            respuesta.error = false
            respuesta.mensaje = "Consulta exitosa."
            listaEstado.each {
                lista.add(
                        "id":it.id,
                        "nombre":it.nombre
                )
            }
        }else{
            respuesta.error = false
            respuesta.mensaje = "Sin registros."
        }
        respuesta.estados = lista
        respond respuesta
    }//def



    /*
     * Permite obtener el listado de los municipios de Morelos o un estado específico.
     */
    def listarMunicipios(){
        def respuesta = [:]
        def criteria = Municipio.createCriteria()
        def listaMunicipio =  criteria.list {
            if(params.region == "seleccionada"){ //Los municipios del estado seleccionado
                estado{
                    'in'("id", params.estadoId.toLong())
                }
            }else if(params.region == "morelos"){ //Solo los municipios de Morelos
                estado{
                    'in'("id", 2444.toLong())
                }
            }
            order("nombre", "asc")
        }
        def lista = []
        if(listaMunicipio){
            respuesta.error = false
            respuesta.mensaje = "Consulta exitosa."
            listaMunicipio.each {
                lista.add(
                        "id":it.id,
                        "nombre":it.nombre
                )
            }
        }else{
            respuesta.error = false
            respuesta.mensaje = "Sin registros."
        }
        respuesta.estados = lista
        respond respuesta
    }//def



    /*
     * Permite registrar o actualizar una modalidad de delito.
     */
    def registrarModalidadDelito(){
        def respuesta = [:]
        def parameters = request.JSON
        println(parameters)
        boolean esRegistro = false
        def existeRegistro = ModalidadDelito.findAllByNombreOrClave(parameters.nombre, parameters.clave)[0]
        def registroId = (existeRegistro) ? existeRegistro.id : null
        def objetoModalidad = (!parameters.id || parameters == "") ? new ModalidadDelito() :  ModalidadDelito.get(parameters.id)
        if(!parameters.id || parameters == ""){ //Registro nuevo
            esRegistro = true
            objetoModalidad.estatus = true
            objetoModalidad.fechaIngreso = new Date()
            objetoModalidad.fechaActualizacion = new Date()
            objetoModalidad.tipoDelito = TipoDelito.get(parameters.tipoDelito.id)
        }else{ //Actualización registro
            objetoModalidad.fechaActualizacion = new Date()
        }
        objetoModalidad.clave = parameters.clave
        objetoModalidad.nombre = parameters.nombre
        if(esRegistro && !existeRegistro){ //Registro
            if (objetoModalidad.save(flush: true, failOnError: true)){
                respuesta.error = false
                respuesta.mensaje = "Registro exitoso."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Registro fallido."
            }
        }else if((!esRegistro && parameters.id == registroId) || (!esRegistro && !existeRegistro)){ //Actualización
            if (objetoModalidad.save(flush: true)){
                respuesta.error = false
                respuesta.mensaje = "Actualización exitosa."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Actualización fallida."
            }
        }else{ //Registro duplicado
            respuesta.error = true
            respuesta.mensaje = "Registro existente, pruebe con otro <<Nombre>> y/o <<Clave>>."
        }
        respond respuesta
    }//def



    /*
     * Permite mostrar el listado de modalidades de delito.
     */
    def listarModalidadesDelito(){
        def respuesta = [:]
        def criteria = ModalidadDelito.createCriteria()
        def listaModalidad = criteria.list {
            //eq("estatus", true)
            tipoDelito{
                'in'("id", params.tipoDelitoId.toLong())
            }
            order("id", "desc")
        }
        respuesta.error = false
        respuesta.mensaje = (listaModalidad) ? "Consulta exitosa." : "Sin registros."
        respuesta.modalidadesDelito = listaModalidad
        respond respuesta
    }//def



    /*
     * Permite cambiar el estatus de una modalidad de delito en específico.
     */
    def actualizarEstatusModalidadDelito(){
        def respuesta = [:]
        def objetoModalidad = ModalidadDelito.get(params.modalidadId)
        if(objetoModalidad){
            objetoModalidad.estatus = (objetoModalidad.estatus) ? false : true
            if(objetoModalidad.save(flush: true, failOnError: true)){
                respuesta.error = false
                respuesta.mensaje = "Actualización exitosa."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Actualización fallida."
            }
        }else{
            respuesta.error = true
            respuesta.mensaje = "Registro inexistente."
        }
        respond respuesta
    }//def



    /*
     * Permite eliminar una modalidad de delito en específico.
     */
    def eliminarModalidadDelito(){
        def respuesta = [:]
        def objetoModalidad = ModalidadDelito.get(params.delitoId)
        println(params)
        if(objetoModalidad){
            if(!objetoModalidad.delete(flush: true, failOnError: true)){
                respuesta.error = false
                respuesta.mensaje = "Eliminación exitosa."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Eliminación fallida."
            }
        }else{
            respuesta.error = true
            respuesta.mensaje = "Registro a eliminar inexistente."
        }
        println(respuesta)
        respond respuesta
    }//def



    /*
     * Permite registrar y actualizar un tipo de delito.
     */
    def registrarTipoDelito(){
        def respuesta = [:]
        def parameters = request.JSON
        println(parameters)
        boolean esRegistro = false
        def existeRegistro = TipoDelito.findAllByNombre(parameters.nombre)[0]
        def registroId = (existeRegistro) ? existeRegistro.id : null
        def objetoTipo = (!parameters.id || parameters == "") ? new TipoDelito() : TipoDelito.get(parameters.id)
        if(!parameters.id || parameters == ""){ //Registro nuevo
            esRegistro = true
            objetoTipo.estatus = true
            objetoTipo.fechaIngreso = new Date()
            objetoTipo.fechaActualizacion = new Date()
        }else{ //Actualización registro
            objetoTipo.fechaActualizacion = new Date()
        }
        objetoTipo.nombre = parameters.nombre
        if(esRegistro && !existeRegistro){ //Registro
            if (objetoTipo.save(flush: true, failOnError: true)){
                respuesta.error = false
                respuesta.mensaje = "Registro exitoso."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Registro fallido."
            }
        }else if((!esRegistro && parameters.id == registroId) || (!esRegistro && !existeRegistro)){ //Actualización
            if (objetoTipo.save(flush: true)){
                respuesta.error = false
                respuesta.mensaje = "Actualización exitosa."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Actualización fallida."
            }
        }else{ //Registro duplicado
            respuesta.error = true
            respuesta.mensaje = "Registro existente, pruebe con otro <<Nombre>>."
        }
        respond respuesta
    }//def



    /*
     * Permite mostrar la lista de los tipos de delito.
     */
    def listarTipoDelitos(){
        def respuesta = [:]
        def criteria = TipoDelito.createCriteria()
        def listaDelito = criteria.list {
            //eq("estatus", true)
            order("id", "desc")
        }
        def lista = []
        if(listaDelito){
            respuesta.error = false
            respuesta.mensaje = "Consulta exitosa."
            listaDelito.each {
                def objetoDelito = [:]
                objetoDelito.id = it.id
                objetoDelito.nombre = it.nombre
                objetoDelito.estatus = it.estatus
                objetoDelito.fechaIngreso = it.fechaIngreso
                objetoDelito.fechaActualizacion = it.fechaActualizacion
                lista << objetoDelito
            }
        }else{
            respuesta.error = false
            respuesta.mensaje = "Sin registros."
        }
        respuesta.delitos = lista
        respond respuesta
    }//def



    /*
     * Permite cambiar el estatus de un tipo de delito en específico.
     */
    def actualizarEstatusTipoDelito(){
        def respuesta = [:]
        def objetoDelito = TipoDelito.get(params.delitoId)
        if(objetoDelito){
            objetoDelito.estatus = (objetoDelito.estatus) ? false : true
            if(objetoDelito.save(flush: true, failOnError: true)){
                respuesta.error = false
                respuesta.mensaje = "Actualización exitosa."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Actualización fallida."
            }
        }else{
            respuesta.error = true
            respuesta.mensaje = "Registro inexistente."
        }
        respond respuesta
    }//def



    /*
     * Permite registrar y actualizar un tipo de libertad
     */
    def registrarTipoLibertad(){
        def respuesta = [:]
        def parameters = request.JSON
        println(parameters)
        boolean esRegistro = false
        def existeRegistro = TipoLibertad.findAllByNombre(parameters.nombre)[0]
        def registroId = (existeRegistro) ? existeRegistro.id : null
        def objetoTipo = (!parameters.id || parameters == "") ? new TipoLibertad() : TipoLibertad.get(parameters.id)
        if(!parameters.id || parameters == ""){ //Registro nuevo
            esRegistro = true
            objetoTipo.estatus = true
            objetoTipo.fechaIngreso = new Date()
            objetoTipo.fechaActualizacion = new Date()
        }else{ //Actualización registro
            objetoTipo.fechaActualizacion = new Date()
        }
        objetoTipo.nombre = parameters.nombre
        if(esRegistro && !existeRegistro){ //Registro
            if (objetoTipo.save(flush: true, failOnError: true)){
                respuesta.error = false
                respuesta.mensaje = "Registro exitoso."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Registro fallido."
            }
        }else if((!esRegistro && parameters.id == registroId) || (!esRegistro && !existeRegistro)){ //Actualización
            if (objetoTipo.save(flush: true)){
                respuesta.error = false
                respuesta.mensaje = "Actualización exitosa."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Actualización fallida."
            }
        }else{ //Registro duplicado
            respuesta.error = true
            respuesta.mensaje = "Registro existente, pruebe con otro <<Nombre>>."
        }
        respond respuesta
    }//def



    /*
     * Permite mostrar el listado de los tipos de libertades
     */
    def listarTipoLibertades(){
        def respuesta = [:]
        def criteria = TipoLibertad.createCriteria()
        def listaTipo = criteria.list {
            //eq("estatus", true)
            order("id", "desc")
        }
        if(listaTipo){
            respuesta.error = false
            respuesta.mensaje = "Consulta exitosa."
        }else{
            respuesta.error = false
            respuesta.mensaje = "Sin registros."
        }
        respuesta.tiposLibertad = listaTipo
        respond respuesta
    }//def



    /*
     * Permite cambiar el estatus de una modalidad de delito en específico.
     */
    def actualizarEstatusTipoLibertad(){
        def respuesta = [:]
        def objetoTipo = TipoLibertad.get(params.tipoId)
        if(objetoTipo){
            objetoTipo.estatus = (objetoTipo.estatus) ? false : true
            if(objetoTipo.save(flush: true, failOnError: true)){
                respuesta.error = false
                respuesta.mensaje = "Actualización exitosa."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Actualización fallida."
            }
        }else{
            respuesta.error = true
            respuesta.mensaje = "Registro inexistente."
        }
        respond respuesta
    }//def



    /*
     * Permite registrar y actualizar una clasificación jurídica
     */
    def registrarClasificacionJuridica(){
        def respuesta = [:]
        def parameters = request.JSON
        println(parameters)
        boolean esRegistro = false
        def existeRegistro = ClasificacionJuridica.findAllByNombreOrClave(parameters.nombre, parameters.clave)[0]
        def registroId = (existeRegistro) ? existeRegistro.id : null
        def objetoClasificacion = (!parameters.id || parameters == "") ? new ClasificacionJuridica() :  ClasificacionJuridica.get(parameters.id)
        if(!parameters.id || parameters == ""){ //Registro nuevo
            esRegistro = true
            objetoClasificacion.estatus = true
            objetoClasificacion.fechaIngreso = new Date()
            objetoClasificacion.fechaActualizacion = new Date()
        }else{ //Actualización registro
            objetoClasificacion.fechaActualizacion = new Date()
        }
        objetoClasificacion.clave = parameters.clave
        objetoClasificacion.nombre = parameters.nombre
        if(esRegistro && !existeRegistro){ //Registro
            if (objetoClasificacion.save(flush: true, failOnError: true)){
                respuesta.error = false
                respuesta.mensaje = "Registro exitoso."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Registro fallido."
            }
        }else if((!esRegistro && parameters.id == registroId) || (!esRegistro && !existeRegistro)){ //Actualización
            if (objetoClasificacion.save(flush: true)){
                respuesta.error = false
                respuesta.mensaje = "Actualización exitosa."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Actualización fallida."
            }
        }else{ //Registro duplicado
            respuesta.error = true
            respuesta.mensaje = "Registro existente, pruebe con otro <<Nombre>> y/o <<Clave>>."
        }
        respond respuesta
    }



    /*
     * Permite mostrar la lista de las clasificaciones jurídicas registradas.
     */
    def listarClasificacionesJuridicas(){
        def respuesta = [:]
        def criteria = ClasificacionJuridica.createCriteria()
        def listaClasificacion = criteria.list {
            //eq("estatus", true)
            order("id", "desc")
        }
        if(listaClasificacion){
            respuesta.error = false
            respuesta.mensaje = "Consulta exitosa."
        }else{
            respuesta.error = false
            respuesta.mensaje = "Sin registros."
        }
        respuesta.clasificacionesJuridicas = listaClasificacion
        respond respuesta
    }//def



    /*
     * Permite cambiar el estatus de una modalidad de delito en específico.
     */
    def actualizarEstatusClasificacionJuridica(){
        def respuesta = [:]
        def objetoClasificacion = ClasificacionJuridica.get(params.clasificacionId)
        if(objetoClasificacion){
            objetoClasificacion.estatus = (objetoClasificacion.estatus) ? false : true
            if(objetoClasificacion.save(flush: true, failOnError: true)){
                respuesta.error = false
                respuesta.mensaje = "Actualización exitosa."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Actualización fallida."
            }
        }else{
            respuesta.error = true
            respuesta.mensaje = "Registro inexistente."
        }
        respond respuesta
    }//def



    /*
     * Permite registrar y actualizar una enfermedad crónica.
     */
    def registrarEnfermedadCronica(){
        def respuesta = [:]
        def parameters = request.JSON
        println(parameters)
        boolean esRegistro = false
        def existeRegistro = EnfermedadCronica.findAllByNombre(parameters.nombre)[0]
        def registroId = (existeRegistro) ? existeRegistro.id : null
        def objetoEnfermedad = (!parameters.id || parameters == "") ? new EnfermedadCronica() :  EnfermedadCronica.get(parameters.id)
        if(!parameters.id || parameters == ""){ //Registro nuevo
            esRegistro = true
            objetoEnfermedad.estatus = true
            objetoEnfermedad.fechaIngreso = new Date()
            objetoEnfermedad.fechaActualizacion = new Date()
        }else{ //Actualización registro
            objetoEnfermedad.fechaActualizacion = new Date()
        }
        objetoEnfermedad.nombre = parameters.nombre
        if(esRegistro && !existeRegistro){ //Registro
            if (objetoEnfermedad.save(flush: true, failOnError: true)){
                respuesta.error = false
                respuesta.mensaje = "Registro exitoso."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Registro fallido."
            }
        }else if((!esRegistro && parameters.id == registroId) || (!esRegistro && !existeRegistro)){ //Actualización
            if (objetoEnfermedad.save(flush: true)){
                respuesta.error = false
                respuesta.mensaje = "Actualización exitosa."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Actualización fallida."
            }
        }else{ //Registro duplicado
            respuesta.error = true
            respuesta.mensaje = "Registro existente, pruebe con otro <<Nombre>>."
        }
        respond respuesta
    }



    /*
     * Permite consultar la lista de las enfermedades crónicas.
     */
    def listarEnfermedadesCronicas(){
        def respuesta = [:]
        def criteria = EnfermedadCronica.createCriteria()
        def listaClasificacion = criteria.list {
            //eq("estatus", true)
            order("id", "desc")
        }
        if(listaClasificacion){
            respuesta.error = false
            respuesta.mensaje = "Consulta exitosa."
        }else{
            respuesta.error = false
            respuesta.mensaje = "Sin registros."
        }
        respuesta.enfermedades = listaClasificacion
        respond respuesta
    }//def



    /*
     * Permite cambiar el estatus de una enfermedad crónica.
     */
    def actualizarEstatusEnfermedadCronica(){
        def respuesta = [:]
        def objetoEnfermedad = EnfermedadCronica.get(params.enfermedadId)
        if(objetoEnfermedad){
            objetoEnfermedad.estatus = (objetoEnfermedad.estatus) ? false : true
            if(objetoEnfermedad.save(flush: true, failOnError: true)){
                respuesta.error = false
                respuesta.mensaje = "Actualización exitosa."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Actualización fallida."
            }
        }else{
            respuesta.error = true
            respuesta.mensaje = "Registro inexistente."
        }
        respond respuesta
    }//def



    /*
     * Permite registrar y actualizar un motivo de reubicación.
     */
    def registrarMotivoReubicacion(){
        def respuesta = [:]
        def parameters = request.JSON
        println(parameters)
        boolean esRegistro = false
        def objetoMotivo = (!parameters.id || parameters == "") ? new MotivoReubicacion() :  MotivoReubicacion.get(parameters.id)
        if(!parameters.id || parameters == ""){ //Registro nuevo
            esRegistro = true
            objetoMotivo.estatus = true
            objetoMotivo.fechaIngreso = new Date()
            objetoMotivo.fechaActualizacion = new Date()
        }else{ //Actualización registro
            objetoMotivo.fechaActualizacion = new Date()
        }
        objetoMotivo.descripcion = parameters.descripcion
        if(objetoMotivo.save(flush: true, failOnError: true)){
            respuesta.error = false
            respuesta.mensaje = (esRegistro) ? "Registro exitoso." : "Actualización exitosa."
        }else{
            respuesta.error = true
            respuesta.mensaje = (esRegistro) ? "Registro fallido." : "Registro fallido."
        }
        respond respuesta
    }



    /*
     * Permite mostrar la lista de motivos de reubicación.
     */
    def listarMotivosReubicacion(){
        def respuesta = [:]
        def criteria = MotivoReubicacion.createCriteria()
        def listaMotivos = criteria.list {
            //eq("estatus", true)
            order("id", "desc")
        }
        if(listaMotivos){
            respuesta.error = false
            respuesta.mensaje = "Consulta exitosa."
        }else{
            respuesta.error = false
            respuesta.mensaje = "Sin registros."
        }
        respuesta.enfermedades = listaMotivos
        respond respuesta
    }



    /*
     * Permite cambiar el estatus de una enfermedad crónica.
     */
    def actualizarEstatusMotivoReubicacion(){
        def respuesta = [:]
        def objetoMotivo = MotivoReubicacion.get(params.motivoId)
        if(objetoMotivo){
            objetoMotivo.estatus = (objetoMotivo.estatus) ? false : true
            if(objetoMotivo.save(flush: true, failOnError: true)){
                respuesta.error = false
                respuesta.mensaje = "Actualización exitosa."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Actualización fallida."
            }
        }else{
            respuesta.error = true
            respuesta.mensaje = "Registro inexistente."
        }
        respond respuesta
    }//def



    /*
     * Permite registrar y actualizar un dormitorio de un centro penitenciario.
     */
    def registrarDormitorio(){
        def respuesta = [:]
        def parameters = request.JSON
        println(parameters)
        boolean esRegistro = false
        def objetoDormitorio = (!parameters.id || parameters == "") ? new Dormitorio() :  Dormitorio.get(parameters.id)
        if(!parameters.id || parameters == ""){ //Registro nuevo
            esRegistro = true
            objetoDormitorio.estatus = true
            objetoDormitorio.fechaIngreso = new Date()
            objetoDormitorio.fechaActualizacion = new Date()
        }else{ //Actualización registro
            objetoDormitorio.fechaActualizacion = new Date()
        }
        objetoDormitorio.clave = parameters.clave
        objetoDormitorio.nombre = parameters.nombre
        objetoDormitorio.capacidadInternos = parameters.capacidadInternos
        objetoDormitorio.ubicacion = parameters.ubicacion
        if(parameters.temporal != null || parameters.temporal == ""){objetoDormitorio.temporal = parameters.temporal}
        objetoDormitorio.centroPenitenciario = CentroPenitenciario.get(parameters.centroPenitenciario.id)
        if(objetoDormitorio.save(flush: true, failOnError: true)){
            respuesta.error = false
            respuesta.mensaje = (esRegistro) ? "Registro exitoso." : "Actualización exitosa."
        }else{
            respuesta.error = true
            respuesta.mensaje = (esRegistro) ? "Registro fallido." : "Registro fallido."
        }
        respond respuesta
    }//def



    /*
     * Permite obtener la lista de los dormitorios de un centro penitenciario en específico.
     */
    def listarDormitorios(){
        def respuesta = [:]
        def criteria = Dormitorio.createCriteria()
        def listaDormitorio = criteria.list {
            centroPenitenciario{
                'in'("id", params.centroId.toLong())
            }
            //eq("estatus", true)
            order("id", "desc")
        }
        def lista = []
        if(listaDormitorio){
            respuesta.error = false
            respuesta.mensaje = "Consulta exitosa."
            listaDormitorio.each {
                def objetoDormitorio = [:]
                objetoDormitorio.id = it.id
                objetoDormitorio.clave = it.clave
                objetoDormitorio.nombre = it.nombre
                objetoDormitorio.capacidadInternos = it.capacidadInternos
                objetoDormitorio.ubicacion = it.ubicacion
                objetoDormitorio.temporal = it.temporal
                objetoDormitorio.estatus = it.estatus
                objetoDormitorio.fechaIngreso = it.fechaIngreso
                objetoDormitorio.fechaActualizacion = it.fechaActualizacion
                def objetoCentro = [:]
                objetoCentro.id = it.centroPenitenciario.id
                objetoCentro.nombre = it.centroPenitenciario.nombre
                objetoDormitorio.centroPenitenciario = objetoCentro
                lista << objetoDormitorio
            }
        }else{
            respuesta.error = false
            respuesta.mensaje = "Sin registros."
        }
        respuesta.dormitorios = lista
        respond respuesta
    }



    /*
     * Permite cambiar el estatus de un dormitorio.
     */
    def actualizarEstatusDormitorio(){
        def respuesta = [:]
        println(params)
        def objetoDormitorio = Dormitorio.get(params.dormitorioId)
        if(objetoDormitorio){
            objetoDormitorio.estatus = (objetoDormitorio.estatus) ? false : true
            println(objetoDormitorio.estatus)
            if(objetoDormitorio.save(flush: true, failOnError: true)){
                respuesta.error = false
                respuesta.mensaje = "Actualización exitosa."
                println(objetoDormitorio.estatus)
            }else{
                respuesta.error = true
                respuesta.mensaje = "Actualización fallida."
            }
        }else{
            respuesta.error = true
            respuesta.mensaje = "Registro inexistente."
        }
        respond respuesta
    }//def



    /*
     * Permite registrar y actualizar un tipo de actividad para un centro penitenciario en específico.
     */
    def registrarTipoActividad(){
        def respuesta = [:]
        def parameters = request.JSON
        println(parameters)
        boolean esRegistro = false
        def objetoTipo = (!parameters.id || parameters == "") ? new TipoActividad() :  TipoActividad.get(parameters.id)
        if(!parameters.id || parameters == ""){ //Registro nuevo
            esRegistro = true
            objetoTipo.estatus = true
            objetoTipo.fechaIngreso = new Date()
            objetoTipo.fechaActualizacion = new Date()
        }else{ //Actualización registro
            objetoTipo.fechaActualizacion = new Date()
        }
        objetoTipo.descripcion = parameters.descripcion
        objetoTipo.centroPenitenciario = CentroPenitenciario.get(parameters.centroPenitenciario.id)
        if(objetoTipo.save(flush: true, failOnError: true)){
            respuesta.error = false
            respuesta.mensaje = (esRegistro) ? "Registro exitoso." : "Actualización exitosa."
        }else{
            respuesta.error = true
            respuesta.mensaje = (esRegistro) ? "Registro fallido." : "Registro fallido."
        }
        respond respuesta
    }//def



    /*
     * Permite obtener la lista de los tipos de actividades de un centro penitenciario en específico.
     */
    def listarTipoActividades(){
        def respuesta = [:]
        def criteria = TipoActividad.createCriteria()
        def listaTipo = criteria.list {
            centroPenitenciario{
                'in'("id", params.centroId.toLong())
            }
            //eq("estatus", true)
            order("id", "desc")
        }
        def lista = []
        if(listaTipo){
            respuesta.error = false
            respuesta.mensaje = "Consulta exitosa."
            listaTipo.each {
                def objetoDormitorio = [:]
                objetoDormitorio.id = it.id
                objetoDormitorio.descripcion = it.descripcion
                objetoDormitorio.estatus = it.estatus
                objetoDormitorio.fechaIngreso = it.fechaIngreso
                objetoDormitorio.fechaActualizacion = it.fechaActualizacion
                def objetoCentro = [:]
                objetoCentro.id = it.centroPenitenciario.id
                objetoCentro.nombre = it.centroPenitenciario.nombre
                objetoDormitorio.centroPenitenciario = objetoCentro
                lista << objetoDormitorio
            }
        }else{
            respuesta.error = false
            respuesta.mensaje = "Sin registros."
        }
        respuesta.tipoActividades = lista
        respond respuesta
    }



    /*
     * Permite cambiar el estatus de un tipo de actividad.
     */
    def actualizarEstatusTipoActividad(){
        def respuesta = [:]
        def objetoTipo = TipoActividad.get(params.tipoId)
        if(objetoTipo){
            objetoTipo.estatus = (objetoTipo.estatus) ? false : true
            if(objetoTipo.save(flush: true, failOnError: true)){
                respuesta.error = false
                respuesta.mensaje = "Actualización exitosa."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Actualización fallida."
            }
        }else{
            respuesta.error = true
            respuesta.mensaje = "Registro inexistente."
        }
        respond respuesta
    }//def



    /*
     * Permite registrar y actualizar una actividad.
     */
    def registrarActividad(){
        def respuesta = [:]
        def parameters = request.JSON
        println(parameters)
        boolean esRegistro = false
        def objetoActividad = (!parameters.id || parameters == "") ? new Actividad() :  Actividad.get(parameters.id)
        if(!parameters.id || parameters == ""){ //Registro nuevo
            esRegistro = true
            objetoActividad.estatus = true
            objetoActividad.fechaIngreso = new Date()
            objetoActividad.fechaActualizacion = new Date()
        }else{ //Actualización registro
            objetoActividad.fechaActualizacion = new Date()
        }
        objetoActividad.descripcion = parameters.descripcion
        objetoActividad.tipoActividad = TipoActividad.get(parameters.tipoActividad.id)
        if(objetoActividad.save(flush: true, failOnError: true)){
            respuesta.error = false
            respuesta.mensaje = (esRegistro) ? "Registro exitoso." : "Actualización exitosa."
        }else{
            respuesta.error = true
            respuesta.mensaje = (esRegistro) ? "Registro fallido." : "Registro fallido."
        }
        respond respuesta
    }//def



    /*
     * Permite registrar y actualizar un actividad de un centro penitenciario en específico
     */
    def listarActividades(){
        def respuesta = [:]
        def criteria = Actividad.createCriteria()
        def listaActividad = criteria.list {
            tipoActividad{
                'in'("id", params.tipoId.toLong())
            }
            //eq("estatus", true)
            order("id", "desc")
        }
        def lista = []
        if(listaActividad){
            respuesta.error = false
            respuesta.mensaje = "Consulta exitosa."
            listaActividad.each {
                def objetoActividad = [:]
                objetoActividad.id = it.id
                objetoActividad.descripcion = it.descripcion
                objetoActividad.estatus = it.estatus
                objetoActividad.fechaIngreso = it.fechaIngreso
                objetoActividad.fechaActualizacion = it.fechaActualizacion
                def objetoTipo = [:]
                objetoTipo.id = it.tipoActividad.id
                objetoTipo.nombre = it.tipoActividad.descripcion
                objetoActividad.tipoActividad = objetoTipo
                lista << objetoActividad
            }
        }else{
            respuesta.error = false
            respuesta.mensaje = "Sin registros."
        }
        respuesta.actividades = lista
        respond respuesta
    }



    /*
     * Permite cambiar el estatus de un tipo de actividad.
     */
    def actualizarEstatusActividad(){
        def respuesta = [:]
        def objetoActividad = Actividad.get(params.actividadId)
        if(objetoActividad){
            objetoActividad.estatus = (objetoActividad.estatus) ? false : true
            if(objetoActividad.save(flush: true, failOnError: true)){
                respuesta.error = false
                respuesta.mensaje = "Actualización exitosa."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Actualización fallida."
            }
        }else{
            respuesta.error = true
            respuesta.mensaje = "Registro inexistente."
        }
        respond respuesta
    }//def



    /*
     * Permite obtener la lista de los tipos de centros penitenciarios.
     */
    def listarTipoCentros(){
        def respuesta = [:]
        def listaTipo = TipoCentro.findAll()
        respuesta.error = false
        respuesta.mensaje = (listaTipo) ? "Consulta exitosa." : "Sin registros."
        respuesta.tipoCentros = listaTipo
        respond respuesta
    }//def



    /*
     * Permite listar los grados de estudio.
     */
    def listarGradosEstudio(){
        def respuesta = [:]
        def criteria = GradoEstudio.createCriteria()
        def listaGrado = criteria.list {
            order("id", "asc")
        }
        respuesta.error = false
        respuesta.mensaje = (listaGrado) ? "Consulta exitosa." : "Sin registros."
        respuesta.gradosEstudio = listaGrado
        respond respuesta
    }//def



    /*
     * Permite mostrar el listado de religiones.
     */
    def listarReligiones(){
        def respuesta = [:]
        def criteria = Religion.createCriteria()
        def listaReligion = criteria.list {
            order("nombre", "asc")
        }
        respuesta.error = false
        respuesta.mensaje = (listaReligion) ? "Consulta exitosa." : "Sin registros."
        respuesta.religiones = listaReligion
        respond respuesta
    }//def



    /*
     * Permite mostrar el listado de estados civiles.
     */
    def listarEstadosCiviles(){
        def respuesta = [:]
        def criteria = EstadoCivil.createCriteria()
        def listaCivil = criteria.list {
            order("id", "asc")
        }
        respuesta.error = false
        respuesta.mensaje = (listaCivil) ? "Consulta exitosa." : "Sin registros."
        respuesta.estadosCiviles = listaCivil
        respond respuesta
    }//def



    /*
     * Permite mostrar el listado de ocupaciones(trabajo)
     */
    def listarOcupaciones(){
        def respuesta = [:]
        def criteria = Ocupacion.createCriteria()
        def listaOcupacion = criteria.list {
            order("nombre", "asc")
        }
        respuesta.error = false
        respuesta.mensaje = (listaOcupacion) ? "Consulta exitosa." : "Sin registros."
        respuesta.ocupaciones = listaOcupacion
        respond respuesta
    }//def



    /*
     * Permite mostrar el listado de roles de usuario.
     */
    def listarRoles(){
        def respuesta = [:]

        def userId = userService.currentUserId()
        def roleList = userService.currentUserRole()
        println("ID: ${userId}")
        println("Roles: ${roleList}")

        def criteria = Role.createCriteria()
        def listaRole = criteria.list {
            order("authority", "asc")
        }
        respuesta.error = false
        respuesta.mensaje = (listaRole) ? "Consulta exitosa." : "Sin registros."
        respuesta.roles = listaRole
        respond respuesta
    }//def

}
