package reinsercionserver.procesos

import catalogosestaticos.Municipio
import catalogosestaticos.TipoCentro
import grails.rest.*
import grails.converters.*
import grails.plugin.springsecurity.annotation.Secured
import procesos.CentroPenitenciario

@Secured(['ROLE_TEST'])
class CentroPenitenciarioController {
	static responseFormats = ['json', 'xml']



    /*
     * Permite registrar o actualizar un centro penitenciario.
     */
    def registrarCentroPenitenciario(){
        def respuesta = [:]
        def parameters = request.JSON
        boolean esRegistro = false
        def existeRegistro = CentroPenitenciario.findByNombre(parameters.nombre)
        def registroId = (existeRegistro) ? existeRegistro.id : null
        def objetoCentro = (!parameters.id || parameters == "") ? new CentroPenitenciario() :  CentroPenitenciario.get(parameters.id)
        if(!parameters.id || parameters == ""){ //Registro nuevo
            esRegistro = true
            objetoCentro.estatus = true
            objetoCentro.fechaIngreso = new Date()
            objetoCentro.fechaActualizacion = new Date()
            //Registrar datos necesarios para la generación del folio
            def hoy = new Date()
            objetoCentro.anioActual = hoy.getYear().toInteger()
            objetoCentro.contador = 0
        }else{ //Actualización registro
            objetoCentro.fechaActualizacion = new Date()
        }
        objetoCentro.nombre = parameters.nombre
        objetoCentro.descripcion = parameters.descripcion
        objetoCentro.colonia = parameters.colonia
        objetoCentro.calleNumero = parameters.calleNumero
        objetoCentro.capacidadInternos = parameters.capacidadInternos
        objetoCentro.numeroAdministrativos = parameters.numeroAdministrativos
        objetoCentro.numeroCustodios = parameters.numeroCustodios
        objetoCentro.municipio = Municipio.get(parameters.municipio.id)
        objetoCentro.tipoCentro = TipoCentro.get(parameters.tipoCentro.id)
        if(esRegistro && !existeRegistro){ //Registro
            try{
                objetoCentro.save(failOnError: true)
                //Generar clave del centro. Ejemplo: 01CPATLF
                String claveId = objetoCentro.id.toString().padLeft(2, "0")
                String claveNombre = objetoCentro.nombre.substring(0, 3).toUpperCase()
                String claveTipo = objetoCentro.tipoCentro.nombre.substring(0, 1).toUpperCase()
                String clave = "${claveId}CP${claveNombre}${claveTipo}"
                println("Clave generada: ${clave}")
                objetoCentro.clave = clave
                //Actualizar la clave del registro
                objetoCentro.save(flush: true, failOnError: true)
                respuesta.error = false
                respuesta.mensaje = "Registro exitoso."
            }catch(Exception e){
                respuesta.error = true
                respuesta.mensaje = "Registro fallido."
            }
        }else if((!esRegistro && parameters.id == registroId) || (!esRegistro && !existeRegistro)){ //Actualización
            if (objetoCentro.save(flush: true, failOnError: true)){
                respuesta.error = false
                respuesta.mensaje = "Actualización exitosa."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Actualización fallida."
            }
        }else{ //Registro duplicado
            respuesta.error = true
            respuesta.mensaje = "Registro existente, pruebe con otro <<Nombre>>."
        }
        respond respuesta
    }



    /*
     * Permite mostrar la lista de centros penitenciarios
     */
    def listarCentrosPenitenciarios(){
        def respuesta = [:]
        def criteria = CentroPenitenciario.createCriteria()
        def listaCentro = criteria.list {
            //eq("estatus", true)
            order("nombre", "asc")
        }
        def lista = []
        if(listaCentro){
            respuesta.error = false
            respuesta.mensaje = "Consulta exitosa."
            listaCentro.each {
                def objetoCentro = [:]
                objetoCentro.id = it.id
                objetoCentro.clave = it.clave
                objetoCentro.nombre = it.nombre
                objetoCentro.descripcion = it.descripcion
                objetoCentro.colonia = it.colonia
                objetoCentro.calleNumero = it.calleNumero
                objetoCentro.capacidadInternos = it.capacidadInternos
                objetoCentro.numeroAdministrativos = it.numeroAdministrativos
                objetoCentro.numeroCustodios = it.numeroCustodios
                objetoCentro.estatus = it.estatus

                def tipoCentro = [:]
                tipoCentro.id = it.tipoCentro.id
                tipoCentro.nombre = it.tipoCentro.nombre
                objetoCentro.tipoCentro = tipoCentro

                def objetoMunicipio = [:]
                objetoMunicipio.id = it.municipio.id
                objetoMunicipio.nombre = it.municipio.nombre
                def objetoEstado = [:]
                objetoEstado.id = it.municipio.estado.id
                objetoEstado.nombre = it.municipio.estado.nombre
                objetoMunicipio.estado = objetoEstado
                objetoCentro.municipio = objetoMunicipio
                lista << objetoCentro
            }
        }else{
            respuesta.error = false
            respuesta.mensaje = "Sin registros."
        }
        respuesta.centros = lista
        respond respuesta
    }//def



    /*
     * Permite cambiar el estatus de una modalidad de delito en específico.
     */
    def actualizarEstatusCentroPenitenciario(){
        def respuesta = [:]
        def objetoCentro = CentroPenitenciario.get(params.centroId)
        if(objetoCentro){
            objetoCentro.estatus = (objetoCentro.estatus) ? false : true
            if(objetoCentro.save(flush: true, failOnError: true)){
                respuesta.error = false
                respuesta.mensaje = "Actualización exitosa."
            }else{
                respuesta.error = true
                respuesta.mensaje = "Actualización fallida."
            }
        }else{
            respuesta.error = true
            respuesta.mensaje = "Registro a eliminar inexistente."
        }
        respond respuesta
    }//def
}
