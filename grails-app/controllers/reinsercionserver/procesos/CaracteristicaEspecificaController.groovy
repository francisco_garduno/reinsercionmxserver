package reinsercionserver.procesos


import grails.rest.*
import grails.converters.*
import grails.plugin.springsecurity.annotation.Secured
import procesos.CaracteristicaEspecifica
import procesos.Imputado

@Secured(['ROLE_ADMIN'])
class CaracteristicaEspecificaController {


    static responseFormats = ['json', 'xml']

    /*
    def registrarCaracteristicasEspecificas() {
        def respuesta = [:]
        def parameters = request.JSON
        boolean esRegistro = false
        def existeRegistro = CaracteristicaEspecifica.findById(parameters.id)
        ​
        def registroId = (existeRegistro) ? existeRegistro.id : null
        println(registroId)
        ​
        def objetoCaracteristica = (!parameters.id || parameters == "") ? new CaracteristicaEspecifica() : CaracteristicaEspecifica.get(parameters.id)
        if (!parameters.id || parameters == "") {
            esRegistro = true
        }
        if (parameters.id && !existeRegistro) {
            respuesta.error = true
            respuesta.mensaje = "No existe el registro en la BD."
        } else {
            objetoCaracteristica.estatura = parameters.estatura
            objetoCaracteristica.complexion = parameters.complexion
            objetoCaracteristica.peso = parameters.peso
            objetoCaracteristica.piel = parameters.piel
            objetoCaracteristica.colorPiel = parameters.colorPiel
            objetoCaracteristica.cantidadCabello = parameters.cantidadCabello
            objetoCaracteristica.colorCabello = parameters.colorCabello
            objetoCaracteristica.formaCabello = parameters.formaCabello
            objetoCaracteristica.calvicie = parameters.calvicie
            objetoCaracteristica.implantacion = parameters.implantacion
            objetoCaracteristica.narizraiz = parameters.narizraiz
            objetoCaracteristica.dorsoNariz = parameters.dorsoNariz
            objetoCaracteristica.anchoNariz = parameters.anchoNariz
            objetoCaracteristica.baseNariz = parameters.baseNariz
            objetoCaracteristica.alturaNariz = parameters.alturaNariz
            objetoCaracteristica.frenteTamano = parameters.frenteTamano
            objetoCaracteristica.frenteInclinacion = parameters.frenteInclinacion
            objetoCaracteristica.frenteAncho = parameters.frenteAncho
            objetoCaracteristica.bocaAncho = parameters.bocaAncho
            objetoCaracteristica.bocaComisuras = parameters.bocaComisuras
            objetoCaracteristica.labiosEspesor = parameters.labiosEspesor
            objetoCaracteristica.labiosAlturanaso = parameters.labiosAlturanaso
            objetoCaracteristica.labiosProminencia = parameters.labiosProminencia
            objetoCaracteristica.ojoColor = parameters.ojoColor
            objetoCaracteristica.ojoForma = parameters.ojoForma
            objetoCaracteristica.ojoTamano = parameters.ojoTamano
            objetoCaracteristica.cejasImplantacion = parameters.cejasImplantacion
            objetoCaracteristica.cejasForma = parameters.cejasForma
            objetoCaracteristica.cejasTamano = parameters.cejasTamano
            objetoCaracteristica.cara = parameters.cara
            objetoCaracteristica.mentonTipo = parameters.mentonTipo
            objetoCaracteristica.mentonForma = parameters.mentonForma
            objetoCaracteristica.mentonInclinacion = parameters.mentonInclinacion
            objetoCaracteristica.orejaForma = parameters.orejaForma
            objetoCaracteristica.orejaHelixOriginal = parameters.orejaHelixOriginal
            objetoCaracteristica.orejaHelixSuperior = parameters.orejaHelixSuperior
            objetoCaracteristica.orejaHelixPosterior = parameters.orejaHelixPosterior
            objetoCaracteristica.orejaHelixAdherencia = parameters.orejaHelixAdherencia
            objetoCaracteristica.lobuloContorno = parameters.lobuloContorno
            objetoCaracteristica.lobuloAdherencia = parameters.lobuloAdherencia
            objetoCaracteristica.lobuloParticularidad = parameters.lobuloParticularidad
            objetoCaracteristica.lobuloDimension = parameters.lobuloDimension
            objetoCaracteristica.sangreTipo = parameters.sangreTipo
            objetoCaracteristica.sangreRH = parameters.sangreRH
            objetoCaracteristica.usaAnteojos = parameters.usaAnteojos
            objetoCaracteristica.imputado = Imputado.get(parameters.imputado.id)
            if (esRegistro && !existeRegistro) { //Registro
                if (objetoCaracteristica.save(flush: true, failOnError: true)) {
                    respuesta.error = false
                    respuesta.mensaje = "Registro exitoso."
                    respuesta.id = objetoCaracteristica.id
                } else {
                    respuesta.error = true
                    respuesta.mensaje = "Registro fallido."
                }
            } else if ((!esRegistro && parameters.id == registroId) || (!esRegistro && !existeRegistro)) {
                //Actualización
                if (objetoCaracteristica.save(flush: true, failOnError: true)) {
                    respuesta.error = false
                    respuesta.mensaje = "Actualización exitosa."
                } else {
                    respuesta.error = true
                    respuesta.mensaje = "Actualización fallida."
                }
            } else { //Registro duplicado
                respuesta.error = true
                respuesta.mensaje = "Registro existente, pruebe con otro valor."
            }
        }
        respond respuesta
    }//def


    def listarCaracteristicasEspecificas() {
        def respuesta = [:]
        def criteria = CaracteristicaEspecifica.createCriteria()
        def listaCaracteristicas = criteria.list {
        }
        if (listaCaracteristicas) {
            respuesta.error = false
            respuesta.mensaje = "Consulta exitosa."
        } else {
            respuesta.error = false
            respuesta.mensaje = "Sin registros."
        }
        respuesta.caracteristicas = listaCaracteristicas
        respond respuesta
    }//def
    ​


    def findByCaracteristicasEspecificas() {
        def respuesta = [:]
        def parameters = request.JSON
        def criteria = CaracteristicaEspecifica.createCriteria()
        def listaCaracteristicas = criteria.list {
            eq('imputado.id', parameters.id.toLong())
        }
        ​
        if (listaCaracteristicas) {
            respuesta.error = false
            respuesta.mensaje = "Consulta exitosa."
        } else {
            respuesta.error = false
            respuesta.mensaje = "Sin registros."
        }
        respuesta.caracteristicas = listaCaracteristicas
        respond respuesta
    }//def
    ​


    def eliminarCaracteristicasEspecificas() {
        def respuesta = [:]
        def objetoCaracteristica = CaracteristicaEspecifica.get(params.caracteristicaId)
        if (objetoCaracteristica) {
            if (!objetoCaracteristica.delete(flush: true, failOnError: true)) {
                respuesta.error = false
                respuesta.mensaje = "Eliminación exitosa."
            } else {
                respuesta.error = true
                respuesta.mensaje = "Eliminación fallida."
            }
        } else {
            respuesta.error = true
            respuesta.mensaje = "Registro a eliminar inexistente."
        }
        respond respuesta
    }//def

    */
}
