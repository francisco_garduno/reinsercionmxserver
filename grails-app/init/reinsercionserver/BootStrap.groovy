package reinsercionserver

import auth.Role
import auth.User
import auth.UserRole
import catalogosestaticos.Estado
import catalogosestaticos.Municipio
import catalogosestaticos.Pais
import catalogosestaticos.TipoCentro

class BootStrap {

    def init = { servletContext ->


        Role.findByAuthority("ROLE_TEST") ?: new Role(
                authority: "ROLE_TEST"
        ).save(failOnError: true)

        Role.findByAuthority("ROLE_ADMIN") ?: new Role(
                authority: "ROLE_ADMIN"
        ).save(failOnError: true)

        Role.findByAuthority("ROLE_CONSULTOR") ?: new Role(
                authority: "ROLE_CONSULTOR"
        ).save(failOnError: true)


        //Roles de usuarios
        Role.findByAuthority("ROLE_ADMINISTRADOR") ?: new Role(
                authority: "ROLE_ADMINISTRADOR"
        ).save(failOnError: true)

        Role.findByAuthority("ROLE_DIRECTOR") ?: new Role(
                authority: "ROLE_DIRECTOR"
        ).save(failOnError: true)

        Role.findByAuthority("ROLE_DACTILOSCOPIA") ?: new Role(
                authority: "ROLE_DACTILOSCOPIA"
        ).save(failOnError: true)

        Role.findByAuthority("ROLE_JURIDICO") ?: new Role(
                authority: "ROLE_JURIDICO"
        ).save(failOnError: true)

        Role.findByAuthority("ROLE_ARCHIVO") ?: new Role(
                authority: "ROLE_ARCHIVO"
        ).save(failOnError: true)

        Role.findByAuthority("ROLE_TRABAJO_SOCIAL") ?: new Role(
                authority: "ROLE_TRABAJO_SOCIAL"
        ).save(failOnError: true)

        Role.findByAuthority("ROLE_SEGURIDAD_CUSTODIA") ?: new Role(
                authority: "ROLE_SEGURIDAD_CUSTODIA"
        ).save(failOnError: true)

        Role.findByAuthority("ROLE_COMITE_TECNICO") ?: new Role(
                authority: "ROLE_COMITE_TECNICO"
        ).save(failOnError: true)

        Role.findByAuthority("ROLE_DIRECCION_TECNICA") ?: new Role(
                authority: "ROLE_DIRECCION_TECNICA"
        ).save(failOnError: true)

        Role.findByAuthority("ROLE_SITE") ?: new Role(
                authority: "ROLE_SITE"
        ).save(failOnError: true)




        User.findByUsername("superadmin") ?: new User(
                username: "superadmin",
                password: "superadmin",
                key2fa: "AX56IJEAQTF7PA47PZI5AE44C5B4D62H"
        ).save(failOnError: true)

        User.findByUsername("administrador") ?: new User(
                username: "administrador",
                password: "administrador",
                key2fa: "AZ56IJEAQTF7PA47PZI5AE44C5B4D62H"
        ).save(failOnError: true)

        User.findByUsername("consultor") ?: new User(
                username: "consultor",
                password: "consultor",
                key2fa: "AP56IJEAQTF7PA47PZI5AE44C5B4D62H"
        ).save(failOnError: true)

        UserRole.findByUserAndRole(User.findByUsername("superadmin"), Role.findByAuthority("ROLE_TEST")) ?: new UserRole(
                user: User.findByUsername("superadmin"),
                role: Role.findByAuthority("ROLE_TEST")
        ).save(failOnError: true)

        UserRole.findByUserAndRole(User.findByUsername("administrador"), Role.findByAuthority("ROLE_ADMIN")) ?: new UserRole(
                user: User.findByUsername("administrador"),
                role: Role.findByAuthority("ROLE_ADMIN")
        ).save(failOnError: true)

        UserRole.findByUserAndRole(User.findByUsername("administrador"), Role.findByAuthority("ROLE_CONSULTOR")) ?: new UserRole(
                user: User.findByUsername("administrador"),
                role: Role.findByAuthority("ROLE_CONSULTOR")
        ).save(failOnError: true)

        UserRole.findByUserAndRole(User.findByUsername("consultor"), Role.findByAuthority("ROLE_CONSULTOR")) ?: new UserRole(
                user: User.findByUsername("consultor"),
                role: Role.findByAuthority("ROLE_CONSULTOR")
        ).save(failOnError: true)

        //********** Tipos de centros penitenciarios
        TipoCentro.findByNombre("Femenil") ?: new TipoCentro(
                nombre: "Femenil"
        ).save(failOnError: true)

        TipoCentro.findByNombre("Varonil") ?: new TipoCentro(
                nombre: "Varonil"
        ).save(failOnError: true)

        //Datos para pruebas
        Pais.findByNombre("México") ?: new Pais(
                id: 152,
                codigoTelefonico: "52",
                clave: "MX",
                nombre: "México",
                estatus: true
        ).save(failOnError: true)

        Estado.findByNombre("Morelos") ?: new Estado(
                id: 2444,
                nombre: "Morelos",
                pais: Pais.findByNombre("México")
        ).save(failOnError: true)

        Estado.findByNombre("Guerrero") ?: new Estado(
                nombre: "Guerrero",
                pais: Pais.findByNombre("México")
        ).save(failOnError: true)

        Municipio.findByNombre("Cuernavaca") ?: new Municipio(
                nombre: "Cuernavaca",
                estado: Estado.findByNombre("Morelos")
        ).save(failOnError: true)

        Municipio.findByNombre("Jiutepec") ?: new Municipio(
                nombre: "Jiutepec",
                estado: Estado.findByNombre("Morelos")
        ).save(failOnError: true)

        Municipio.findByNombre("Emiliano Zapata") ?: new Municipio(
                nombre: "Emiliano Zapata",
                estado: Estado.findByNombre("Morelos")
        ).save(failOnError: true)

        Municipio.findByNombre("Temixco") ?: new Municipio(
                nombre: "Temixco",
                estado: Estado.findByNombre("Morelos")
        ).save(failOnError: true)

        Municipio.findByNombre("Iguala") ?: new Municipio(
                nombre: "Iguala",
                estado: Estado.findByNombre("Guerrero")
        ).save(failOnError: true)

        Municipio.findByNombre("Teloloapan") ?: new Municipio(
                nombre: "Teloloapan",
                estado: Estado.findByNombre("Guerrero")
        ).save(failOnError: true)

        Municipio.findByNombre("Apaxtla") ?: new Municipio(
                nombre: "Apaxtla",
                estado: Estado.findByNombre("Guerrero")
        ).save(failOnError: true)

        Municipio.findByNombre("Papanoa") ?: new Municipio(
                nombre: "Papanoa",
                estado: Estado.findByNombre("Guerrero")
        ).save(failOnError: true)

    }
    def destroy = {
    }
}
