package reinsercionserver

import auth.User
import com.google.zxing.client.j2se.MatrixToImageWriter
import com.google.zxing.common.BitMatrix
import de.taimos.totp.TOTP
import com.google.zxing.*
import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.userdetails.DefaultPreAuthenticationChecks
import org.apache.commons.codec.binary.Base32
import org.apache.commons.codec.binary.Hex
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes

import javax.servlet.http.HttpServletRequest
import java.security.SecureRandom

@Transactional
class TowFactorAuthService extends DefaultPreAuthenticationChecks {

    void check(UserDetails user) {

        // do the standard checks
        super.check user

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest()

        def u = User.findByUsername((user.getUsername()))
        String secretKey = u.key2fa
        String code = request.properties.parameterMap?.code[0].toString()

        if (!code.equals(getTOTPCode(secretKey)) ){
            log.debug("Wrong credentials")
            throw new UsernameNotFoundException("Wrong credentials");
        }

    }

    def generateSecretKey() {
        SecureRandom random = new SecureRandom();
        byte[] bytes = new byte[20];
        random.nextBytes(bytes);
        Base32 base32 = new Base32();
        return base32.encodeToString(bytes);
    }

    def getTOTPCode(String secretKey) {
        Base32 base32 = new Base32();
        byte[] bytes = base32.decode(secretKey);
        String hexKey = Hex.encodeHexString(bytes);
        return TOTP.getOTP(hexKey);
    }

    def getGoogleAuthenticatorBarCode(String secretKey, String account, String issuer) {
        try {
            return "otpauth://totp/"+ URLEncoder.encode(issuer + ":" + account, "UTF-8").replace("+", "%20")+ "?secret=" + URLEncoder.encode(secretKey, "UTF-8").replace("+", "%20")+ "&issuer=" + URLEncoder.encode(issuer, "UTF-8").replace("+", "%20")
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e);
        }
    }

    def createQRCode(String barCodeData, int height, int width) throws WriterException, IOException {
        BitMatrix matrix = new MultiFormatWriter().encode(barCodeData, BarcodeFormat.QR_CODE, width, height);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        MatrixToImageWriter.writeToStream(matrix, "png", bos)
        return Base64.getEncoder().encodeToString(bos.toByteArray())
    }
}
