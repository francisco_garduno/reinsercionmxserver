package reinsercionserver

import grails.gorm.transactions.Transactional

@Transactional
class UserService {

    def springSecurityService

    def serviceMethod() {

    }

    def currentUserRole() {
        return springSecurityService.getPrincipal().authorities
    }

    def currentUserId() {
        return springSecurityService.getPrincipal().getId()
    }
}
