import auth.UserPasswordEncoderListener
import reinsercionserver.TowFactorAuthService

// Place your Spring DSL code here
beans = {
    userPasswordEncoderListener(UserPasswordEncoderListener)
    //preAuthenticationChecks(TowFactorAuthService) --- Se quito porque se eliminó el segundo factor
}
