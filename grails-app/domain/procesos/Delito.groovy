package procesos

import catalogosgestionables.ModalidadDelito
import catalogosgestionables.TipoDelito

class Delito {

    Date fechaDetencion
    Date fechaRegistro
    String causaPenal
    String carpetaInvestigacion

    static belongsTo = [imputado: Imputado, tipoDelito: TipoDelito]

    static constraints = {
        fechaDetencion nullable: true, blank: true
        fechaRegistro nullable: true, blank: true
        causaPenal nullable: true, blank: true
        carpetaInvestigacion nullable: true, blank: true
        imputado nullable: false, blank: false
        tipoDelito nullable: false, blank: false
    }
}
