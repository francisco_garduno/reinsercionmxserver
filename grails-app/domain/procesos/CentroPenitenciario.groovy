package procesos

import catalogosestaticos.Municipio
import catalogosestaticos.TipoCentro

class CentroPenitenciario {

    String clave
    String nombre
    String descripcion
    String colonia
    String calleNumero
    int capacidadInternos
    int numeroAdministrativos
    int numeroCustodios
    boolean estatus
    Date fechaIngreso
    Date fechaActualizacion

    static belongsTo = [municipio: Municipio, tipoCentro: TipoCentro]

    //Datos para generación de folios con formato #-Año. Ejemplo: 145-2020
    int anioActual
    int contador

    static constraints = {
        clave nullable: true, blank: true, size: 1..8
        nombre nullable: false, blank: false, unique: true, size: 1..45
        descripcion nullable: false, blank: false, size: 1..255
        colonia nullable: false, blank: false, size: 1..45
        calleNumero nullable: false, blank: false, size: 1..45
        capacidadInternos nullable: true, blank: true
        numeroAdministrativos nullable: true, blank: true
        numeroCustodios nullable: true, blank: true
        estatus nullable: false, blank: false
        fechaIngreso nullable: false, blank: false
        fechaActualizacion nullable: false, blank: false
        municipio nullable: false, blank: false
        tipoCentro nullable: false, blank: false
        anioActual nullable: false, blank: false
        contador nullable: false, blank: false
    }
}
