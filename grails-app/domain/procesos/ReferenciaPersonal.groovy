package procesos

import catalogosestaticos.Municipio
import catalogosestaticos.Ocupacion
import catalogosestaticos.Pais
import catalogosestaticos.Parentesco

class ReferenciaPersonal {

    String nombre
    String apellidoPaterno
    String apellidoMaterno
    String genero
    boolean estaVivo
    String telefono
    int numeroDependientes
    String calleNumero
    String colonia
    String codigoPostal

    static belongsTo = [paisNacimiento: Pais, municipio: Municipio, ocupacion: Ocupacion, parentesco: Parentesco, imputado: Imputado]

    static constraints = {
        nombre nullable: false, blank: false, size: 1..45
        apellidoPaterno nullable: false, blank: false, size: 1..45
        apellidoMaterno nullable: true, blank: true, size: 1..45
        genero nullable: false, blank: false, size: 1..10
        estaVivo nullable: false, blank: false
        telefono nullable: false, blank: false
        numeroDependientes nullable: false, blank: false
        calleNumero nullable: true, blank: true, size: 1..45
        colonia nullable: true, blank: true, size: 1..45
        codigoPostal nullable: true, blank: true, size: 1..5
        paisNacimiento nullable: true, blank: true
        municipio nullable: true, blank: true
        ocupacion nullable: true, blank: true
        parentesco nullable: false, blank: false
    }
}
