package procesos

class CaracteristicaEspecifica {

    int estatura
    String complexion
    double peso
    String piel
    String colorPiel
    String cantidadCabello
    String colorCabello
    String formaCabello
    String calvicie
    String implantacion
    String narizraiz
    String dorsoNariz
    String anchoNariz
    String baseNariz
    String alturaNariz
    String frenteTamano
    String frenteInclinacion
    String frenteAncho
    String bocaAncho
    String bocaComisuras
    String labiosEspesor
    String labiosAlturanaso
    String labiosProminencia
    String ojoColor
    String ojoForma
    String ojoTamano
    String cejasImplantacion
    String cejasForma
    String cejasTamano
    String cara
    String mentonTipo
    String mentonForma
    String mentonInclinacion
    String orejaForma
    String orejaHelixOriginal
    String orejaHelixSuperior
    String orejaHelixPosterior
    String orejaHelixAdherencia
    String lobuloContorno
    String lobuloAdherencia
    String lobuloParticularidad
    String lobuloDimension
    String sangreTipo
    String sangreRH
    boolean usaAnteojos

    static belongsTo = [imputado: Imputado]

    static constraints = {
        estatura nullable: true, blank: false, unique: false, size: 1..4
        complexion nullable: true, blank: false, unique: false, size: 1..20
        peso nullable: true, blank: false, unique: false
        piel nullable: true, blank: false, unique: false, size: 1..20
        colorPiel nullable: true, blank: false, unique: false, size: 1..30
        cantidadCabello nullable: true, blank: false, unique: false, size: 1..30
        colorCabello nullable: true, blank: false, unique: false, size: 1..30
        formaCabello nullable: true, blank: false, unique: false, size: 1..15
        calvicie nullable: true, blank: false, unique: false, size: 1..15
        implantacion nullable: true, blank: false, unique: false, size: 1..15
        narizraiz nullable: true, blank: false, unique: false, size: 1..15
        dorsoNariz nullable: true, blank: false, unique: false, size: 1..15
        anchoNariz nullable: true, blank: false, unique: false, size: 1..15
        baseNariz nullable: true, blank: false, unique: false, size: 1..15
        alturaNariz nullable: true, blank: false, unique: false, size: 1..15
        frenteTamano nullable: true, blank: false, unique: false, size: 1..15
        frenteInclinacion nullable: true, blank: false, unique: false, size: 1..15
        frenteAncho nullable: true, blank: false, unique: false, size: 1..15
        bocaAncho nullable: true, blank: false, unique: false, size: 1..15
        bocaComisuras nullable: true, blank: false, unique: false, size: 1..15
        labiosEspesor nullable: true, blank: false, unique: false, size: 1..15
        labiosAlturanaso nullable: true, blank: false, unique: false, size: 1..15
        labiosProminencia nullable: true, blank: false, unique: false, size: 1..20
        ojoColor nullable: true, blank: false, unique: false, size: 1..15
        ojoForma nullable: true, blank: false, unique: false, size: 1..15
        ojoTamano nullable: true, blank: false, unique: false, size: 1..15
        cejasImplantacion nullable: true, blank: false, unique: false, size: 1..15
        cejasForma nullable: true, blank: false, unique: false, size: 1..30
        cejasTamano nullable: true, blank: false, unique: false, size: 1..15
        cara nullable: true, blank: false, unique: false, size: 1..15
        mentonTipo nullable: true, blank: false, unique: false, size: 1..15
        mentonForma nullable: true, blank: false, unique: false, size: 1..15
        mentonInclinacion nullable: true, blank: false, unique: false, size: 1..15
        orejaForma nullable: true, blank: false, unique: false, size: 1..15
        orejaHelixOriginal nullable: true, blank: false, unique: false, size: 1..15
        orejaHelixSuperior nullable: true, blank: false, unique: false, size: 1..15
        orejaHelixPosterior nullable: true, blank: false, unique: false, size: 1..15
        orejaHelixAdherencia nullable: true, blank: false, unique: false, size: 1..15
        lobuloContorno nullable: true, blank: false, unique: false, size: 1..15
        lobuloAdherencia nullable: true, blank: false, unique: false, size: 1..15
        lobuloParticularidad nullable: true, blank: false, unique: false, size: 1..15
        lobuloDimension nullable: true, blank: false, unique: false, size: 1..15
        sangreTipo nullable: true, blank: false, unique: false, size: 1..6
        sangreRH nullable: true, blank: false, unique: false, size: 1..15
        usaAnteojos nullable: true, blank: false, unique: false
    }
}
