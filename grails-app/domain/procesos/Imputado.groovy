package procesos

import catalogosestaticos.EstadoCivil
import catalogosestaticos.GradoEstudio
import catalogosestaticos.Municipio
import catalogosestaticos.Ocupacion
import catalogosestaticos.Pais
import catalogosestaticos.Religion

class Imputado extends CarpetaImputado {

    String curp
    Date fechaNacimiento
    int edadAparente
    String genero
    String codigoPostal
    String colonia
    String calleNumero
    int numeroHijos
    String nombrePadre
    String nombreMadre
    String etnia
    boolean esIndigina
    boolean hablaIndigena

    static belongsTo = [
            municipio: Municipio,
            paisNacimiento: Pais,
            religion: Religion,
            estadoCivil: EstadoCivil,
            ocupacion: Ocupacion,
            gradoEstudio: GradoEstudio
    ]

    static constraints = {
        curp nullable: true, blank: true, size: 1..18
        fechaNacimiento nullable: false, blank: false
        edadAparente nullable: false, blank: false
        genero nullable: false, blank: false, size: 1..10
        codigoPostal nullable: false, blank: false, size: 1..5
        colonia nullable: false, blank: false, size: 1..45
        calleNumero nullable: false, blank: false, size: 1..45
        numeroHijos nullable: false, blank: false
        nombrePadre nullable: true, blank: true, size: 1..80
        nombreMadre nullable: true, blank: true, size: 1..80
        etnia nullable: true, blank: true, size: 1..45
        esIndigina nullable: false, blank: false
        hablaIndigena nullable: false, blank: false
        municipio nullable: false, blank: false
        paisNacimiento nullable: false, blank: false
        religion nullable: false, blank: false
        estadoCivil nullable: false, blank: false
        ocupacion nullable: false, blank: false
        gradoEstudio nullable: false, blank: false

    }
}
