package procesos


class CarpetaImputado {

    String folio
    String numeroExpediente
    String numeroControlRenip
    String tipoImputado
    String tipoIngreso
    String clasificacion

    static belongsTo = [centroPenitenciario: CentroPenitenciario]

    static constraints = {
        folio nullable: true, blank: true, size: 1..45
        numeroExpediente nullable: true, blank: true, size: 1..45
        numeroControlRenip nullable: true, blank: true, size: 1..45
        tipoImputado nullable: false, blank: false, size: 1..10
        tipoIngreso nullable: false, blank: false, size: 1..20
        clasificacion nullable: true, blank: true, size: 1..10
        centroPenitenciario nullable: false, blank: false
    }

    static mapping = {
        tablePerHierarchy false
    }
}
