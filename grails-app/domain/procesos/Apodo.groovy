package procesos

class Apodo {

    String nombre
    String apellidoPaterno
    String apellidoMaterno
    Date fechaRegistro
    String otro
    boolean principal

    static belongsTo = [imputado: Imputado]

    static constraints = {
        nombre nullable: true, blank: true, size: 1..45
        apellidoPaterno nullable: true, blank: true, size: 1..45
        apellidoMaterno nullable: true, blank: true, size: 1..45
        fechaRegistro nullable: true, blank: true
        otro nullable: true, blank: true, size: 1..60
        principal nullable: false, blank: false
    }
}
