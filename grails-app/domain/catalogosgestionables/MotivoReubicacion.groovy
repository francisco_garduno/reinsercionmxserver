package catalogosgestionables

class MotivoReubicacion {

    String descripcion
    boolean estatus
    Date fechaIngreso
    Date fechaActualizacion

    static constraints = {
        descripcion nullable: false, blank: false
        estatus nullable: false, blank: false
        fechaIngreso nullable: false, blank: false
        fechaActualizacion nullable: false, blank: false
    }
}
