package catalogosgestionables

class TipoDelito {

    String nombre
    boolean estatus
    Date fechaIngreso
    Date fechaActualizacion

    static constraints = {
        nombre nullable: false, blank: false, unique: true, size: 1..45
        estatus nullable: false, blank: false
        fechaIngreso nullable: false, blank: false
        fechaActualizacion nullable: false, blank: false
    }
}
