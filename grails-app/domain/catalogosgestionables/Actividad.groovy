package catalogosgestionables

class Actividad {

    String descripcion
    boolean estatus
    Date fechaIngreso
    Date fechaActualizacion

    static belongsTo = [tipoActividad: TipoActividad]

    static constraints = {
        descripcion nullable: false, blank: false, size: 1..255
        estatus nullable: false, blank: false
        fechaIngreso nullable: false, blank: false
        fechaActualizacion nullable: false, blank: false
        tipoActividad nullable: false, blank: false
    }
}