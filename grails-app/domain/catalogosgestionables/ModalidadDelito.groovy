package catalogosgestionables

class ModalidadDelito {

    String clave
    String nombre
    boolean estatus
    Date fechaIngreso
    Date fechaActualizacion

    static belongsTo = [tipoDelito: TipoDelito]

    static constraints = {
        clave nullable: false, blank: false, unique: true, size: 1..10
        nombre nullable: false, blank: false, unique: true, size: 1..45
        estatus nullable: false, blank: false
        fechaIngreso nullable: false, blank: false
        fechaActualizacion nullable: false, blank: false
        tipoDelito nullable: false, blank: false
    }
}
