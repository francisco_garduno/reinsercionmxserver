package catalogosgestionables

class ClasificacionJuridica {

    String clave
    String nombre
    boolean estatus
    Date fechaIngreso
    Date fechaActualizacion

    static constraints = {
        clave nullable: false, blank: false, size: 1..10, unique: true
        nombre nullable: false, blank: false, size: 1..45, unique: true
        estatus nullable: false, blank: false
        fechaIngreso nullable: false, blank: false
        fechaActualizacion nullable: false, blank: false
    }
}
