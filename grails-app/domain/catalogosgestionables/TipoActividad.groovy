package catalogosgestionables

import procesos.CentroPenitenciario

class TipoActividad {

    String descripcion
    boolean estatus
    Date fechaIngreso
    Date fechaActualizacion

    static belongsTo = [centroPenitenciario: CentroPenitenciario]

    static constraints = {
        descripcion nullable: false, blank: false, size: 1..255
        estatus nullable: false, blank: false
        fechaIngreso nullable: false, blank: false
        fechaActualizacion nullable: false, blank: false
        centroPenitenciario nullable: false, blank: false
    }
}
