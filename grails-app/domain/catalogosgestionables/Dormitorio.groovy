package catalogosgestionables

import procesos.CentroPenitenciario

class Dormitorio {

    String clave
    String nombre
    int capacidadInternos
    String ubicacion
    boolean temporal
    boolean estatus
    Date fechaIngreso
    Date fechaActualizacion

    static belongsTo = [centroPenitenciario: CentroPenitenciario]

    static constraints = {
        clave nullable: true, blank: true, size: 1..3
        nombre nullable: false, blank: false, size: 1..45
        capacidadInternos nullable: false, blank: false
        ubicacion nullable: false, blank: false, size: 1..100
        temporal nullable: true, blank: true
        estatus nullable: false, blank: false
        fechaIngreso nullable: false, blank: false
        fechaActualizacion nullable: false, blank: false
    }
}