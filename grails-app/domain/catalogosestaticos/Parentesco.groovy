package catalogosestaticos

class Parentesco {

    String nombre

    static constraints = {
        nombre nullable: false, blank: false, size: 1..45
    }
}
