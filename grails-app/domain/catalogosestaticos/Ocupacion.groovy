package catalogosestaticos

class Ocupacion {

    String nombre

    static constraints = {
        nombre nullable: false, blank: false, size: 1..60
    }
}
