package catalogosestaticos

class Pais {

    String nombre
    String clave
    int codigoTelefonico
    boolean estatus

    static constraints = {
        nombre nullable: false, blank: false, unique: true, size: 1..80
        clave nullable: false, blank: false, unique: true, size: 1..3
        codigoTelefonico nullable: false, blank: false
        estatus nullable: false, blank: false
    }
}
