package catalogosestaticos

class Municipio {

    String nombre

    static belongsTo = [estado: Estado]

    static constraints = {
        nombre nullable: false, blank: false, size: 1..255
        estado nullable: false, blank: false
    }
}
