package catalogosestaticos

class Estado {

    String nombre

    static belongsTo = [pais: Pais]

    static constraints = {
        nombre nullable: false, blank: false, size: 1..255
        pais nullable: false, blank: false
    }
}
