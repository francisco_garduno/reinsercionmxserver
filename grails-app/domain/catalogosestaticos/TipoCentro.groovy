package catalogosestaticos

class TipoCentro {

    String nombre

    static constraints = {
        nombre nullable: false, blank: false, size: 1..10
    }
}
