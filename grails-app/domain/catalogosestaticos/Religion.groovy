package catalogosestaticos

class Religion {

    String nombre

    static constraints = {
        nombre nullable: false, blank: false, size: 1..45, unique: true
    }
}
